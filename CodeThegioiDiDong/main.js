const express = require('express')
const app = express()
const port = 3000

const userControl = require('./controllers/userController');

// app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.engine('html', require('ejs').renderFile);
app.get('/',userControl.homepage);
app.get('/cart',userControl.cart);
      
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})